provider "aws" {
  region = var.region
}
 
data "aws_availability_zones" "available" {}
 
locals {
  cluster_name = "dso-cicd-${random_string.suffix.result}"
}
 
resource "random_string" "suffix" {
  length  = 8
  special = false
}
 
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"
 
  name = "oir9950-prod-us-east-1"
 
  cidr = "10.216.16.0/21"
  azs  = slice(data.aws_availability_zones.available.names, 0, 3)
 
  private_subnets = ["10.216.17.0/24", "10.216.16.0/24"]
  public_subnets  = ["10.216.19.0/24", "10.216.18.0/24"]
 
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
 
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = 1
    "agreement"                                   = "oir9950"
    "Name"                                        = "oir9950-prod-us-east-1b-pub"
    "environment"                                 = "prod"
  }
 
  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = 1
    "agreement"                                   = "oir9950"
    "Name"                                        = "oir9950-prod-us-east-1b-priv"
    "environment"                                 = "prod"
  }
}

resource "aws_launch_template" "DevOps3" {
  name = "DevOps3"
  
  instance_type = "t3.small"
  key_name      = "Keys-Rancher"
  vpc_security_group_ids = ["sg-0f31c62b8e82f4b92, "sg-071acd55965433b7a"]

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size    = 80
      iops           = 3000
      type           = "gp3"
      throughput     = 125
    }
    
    tag_specifications {
      resource_type  = "instance"

      tags {
        Name = "DevOps3-oir9950-node"
        alpha.eskctl.io/nodegroup-name = "oir9950-nodegroup-1"
        alpha.eskctl.io/nodegroup-type = "managed"
      }
    }
  }


module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.5.1"
 
  cluster_name    = local.cluster_name
  cluster_version = "1.24"
 
  vpc_id                         = module.vpc.vpc_id
  subnet_ids                     = module.vpc.private_subnets
  cluster_endpoint_public_access = true
 
  eks_managed_node_group_defaults = {
    ami_type = "AL2_x86_64"
  }
 
  eks_managed_node_groups = {
    name = "oir9950-nodegroup-1"
    aws_launch_template = "DevOps3"
    min_size     = 2
    max_size     = 3
    desired_size = 2
  }
}
 
data "aws_iam_policy" "ebs_csi_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
}

module "irsa-ebs-csi" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "4.7.0"
 
  create_role                   = true
  role_name                     = "AmazonEKSTFEBSCSIRole-${module.eks.cluster_name}"
  provider_url                  = module.eks.oidc_provider
  role_policy_arns              = [data.aws_iam_policy.ebs_csi_policy.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:ebs-csi-controller-sa"]
}
 
resource "aws_eks_addon" "ebs-csi" {
  cluster_name             = module.eks.cluster_name
  addon_name               = "aws-ebs-csi-driver"
  addon_version            = "v1.5.2-eksbuild.1"
  service_account_role_arn = module.irsa-ebs-csi.iam_role_arn
  tags = {
    "eks_addon" = "ebs-csi"
    "terraform" = "true"
  }
}
 
